let http = require("http");

//Mock database
	let directory=[

		{
			"name" : "Brandon",
			"email" : "brandon@mail.com"
		},
		{
			"name" : "Jobert",
			"email" : "jobert@mail.com"
		}

		]

	const server = http.createServer((request,response)=>{
		if(request.url == "/users" && request.method == "GET"){
			response.writeHead(200,{'Content-Type' : 'application/json'});

			// Input has to be data type STRING hence the JSON.stringify() method
			//This string input will be converted to the desired output data type which has been set to JSON
			//when sending data to a web server, the data has to be string
			response.write(JSON.stringify(directory));
			response.end();
		}

		//add user to the database
		if (request.url == "/users" && request.method == "POST") {

			//Declare and initialize a "requestBody" variable to an empty string
			//This will act as a placeholder for the data to be created later on

			let requestBody = "";

			//A stream is a sequence of data
			//Data is recieved from the client and is processed in the "data stream"
			//The information provided from the request object enters a sequence called "data" the code below will be triggered 
			//data step - this reads the "data" stream and process as the request body

			request.on('data', function(data){

			//Assigns the data retrieved from the data stream to the requestBody
			requestBody +=data;

			});
			//response end step - only runs aftter the request has completely been sent
			request.on('end', function(){

				console.log(typeof requestBody);

				requestBody = JSON.parse(requestBody);

				let newUser = {

				//create new object representing the new database record
					"name" : requestBody.name,
					"email" : requestBody.email
				}

				//Add the new USER into the mock databae 

				directory.push(newUser)
				console.log(directory);

				response.writeHead(200,{'Content-Type': 'application/json'});
				response.write(JSON.stringify(newUser));
				response.end();

			});
		}

	}).listen(4000)

	console.log('Server running at localhost 4000');